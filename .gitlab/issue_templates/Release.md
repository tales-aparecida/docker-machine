For versioning we use `X.Y.Z-gitlab.G-cki.C` pattern, where:

- `X.Y.Z` is directly taken from the upstream version. For now we add
  all our changes on top of `0.16.2` which is the last stable tag of
  Docker Machine.
- `G` is the incremental number increased for each of the GitLab releases.
- `C` is the incremental number increased for each of the CKI releases.

### Release 0.16.2-gitlab.{{G}}-cki.{{C}}

- [ ] `git checkout main && git pull`
- [ ] Increment the value of {{C}} by checking the value defined in https://gitlab.com/cki-project/docker-machine/-/blob/main/version/version.go#L9.
- [ ] Update [version/version.go](https://gitlab.com/cki-project/docker-machine/-/blob/main/version/version.go#L9) to `0.16.2-gitlab.{{G}}-cki.{{C}}`.
- [ ] Add file `git add version/version.go`
- [ ] Commit `git commit -m "Bump version to 0.16.2-gitlab.{{G}}-cki.{{C}}"`
- [ ] Create git tag `git tag -s v0.16.2-gitlab.{{G}}-cki.{{C}} -m "Version 0.16.2-gitlab.{{G}}-cki.{{C}}"`
- [ ] Push tag `git push origin v0.16.2-gitlab.{{G}}-cki.{{C}}`
- [ ] Push to main `git push origin main`
